# Copyright 2015-2022 Robert Schmidl
#
#    Licensed under the Apache License, Version 2.0 (the "License");
#    you may not use this file except in compliance with the License.
#    You may obtain a copy of the License at
#
#        http://www.apache.org/licenses/LICENSE-2.0
#
#    Unless required by applicable law or agreed to in writing, software
#    distributed under the License is distributed on an "AS IS" BASIS,
#    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#    See the License for the specific language governing permissions and
#    limitations under the License.

require 'httpclient'
require 'json'

class CouchDb
  DATABASE_NAME = 'statastic'

  def initialize(base_url)
    @base_url = base_url
    @clnt = HTTPClient.new
    @clnt.set_auth(base_url, ENV['COUCHDB_USER'], ENV['COUCHDB_PASSWORD'])
    @clnt.ssl_config.cert_store.set_default_paths
  end

  def create_database_if_not_exist
    response = @clnt.get("#{@base_url}/#{DATABASE_NAME}")
    if response.nil?
      raise "response is nil"
    end
    if response.status >= 400
      if response.status == 404
        puts "creating database"
        response = @clnt.put("#{@base_url}/#{DATABASE_NAME}")
        if response.nil?
          raise "response is nil"
        end
        if response.status >= 400
          raise "Status code #{response.status}"
        end
      else
        raise "Status code #{response.status}"
      end
    end
  end

  def upload_document(name, content)
    response = @clnt.put("#{@base_url}/#{DATABASE_NAME}/#{name}", content)
    if response.status >= 400
      raise "Status code #{response.status}"
    end
  end

  def create_or_update_document(name, json_document)
    puts "create_or_update_document"
    response = @clnt.head("#{@base_url}/#{DATABASE_NAME}/#{name}")

    if response.nil?
      raise "Response is nil"
    end

    case response.code
    when 404
      puts "response.code is #{response.code}, document does not exist yet"
    when 200
      # set _rev for optimistic locking / concurrency checks
      json_document["_rev"] = response.header['etag'][0].delete '"'
    else
      puts "unhandled response code #{response.code}"
    end
    upload_document( name, JSON.pretty_generate(json_document))
  end
end


couchdb = CouchDb.new(ENV['COUCHDB_URL'])
couchdb.create_database_if_not_exist

Dir.glob("tmp/**/*.json")
   .each{|f|
    puts f
    file = File.read(f)
    json_document = JSON.parse(file)
    couchdb.create_or_update_document( f.sub(/.*\//, '').sub('.json', ''), json_document)
}