# Copyright 2015-2022 Robert Schmidl
#
#    Licensed under the Apache License, Version 2.0 (the "License");
#    you may not use this file except in compliance with the License.
#    You may obtain a copy of the License at
#
#        http://www.apache.org/licenses/LICENSE-2.0
#
#    Unless required by applicable law or agreed to in writing, software
#    distributed under the License is distributed on an "AS IS" BASIS,
#    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#    See the License for the specific language governing permissions and
#    limitations under the License.

require 'httpclient'
require 'json'
require 'influxdb-client'

class Influx

  def initialize
    @clnt = HTTPClient.new
    @clnt.default_header = {'Authorization' =>  "Token #{ENV['FLUX_TOKEN']}"}

    @client = InfluxDB2::Client.new("https://influxdb.statastic.#{ENV['STAGE']}",
                                  ENV['FLUX_TOKEN'],
                                  read_timeout: 180,
                                  precision: InfluxDB2::WritePrecision::SECOND)
    @write_api = @client.create_write_api
  end

  def get_bucket_id name
    response = @clnt.get("https://influxdb.statastic.#{ENV['STAGE']}/api/v2/buckets?name=#{name}")
    unless response.code == 200
      raise "Response != 200: #{response.body}"
    end
    response_hash = JSON.parse(response.body)
    if response_hash['buckets'].length == 0
      return nil
    end
    response_hash['buckets'][0]['id']
  end

  def create_bucket name, description
    id = get_bucket_id name
    if id.nil?
      org_id = get_org_id "statastic"
      response = @clnt.post(
        "https://influxdb.statastic.#{ENV['STAGE']}/api/v2/buckets",
        %{
          {
            "description": "#{description}",
            "name": "#{name}",
            "orgID": "#{org_id}",
            "retentionRules": [],
            "schemaType": "implicit"
          }
        })
      unless response.code == 201
        raise "Response != 201: #{response.body}"
      end
      response_hash = JSON.parse(response.body)
      id = response_hash['id']
    end
    id
  end

  def get_org_id name
    response = @clnt.get("https://influxdb.statastic.#{ENV['STAGE']}/api/v2/orgs?name=#{name}")
    unless response.code == 200
      raise "Response != 200: #{response.body}"
    end
    response_hash = JSON.parse(response.body)
    if response_hash['orgs'].length == 0
      return nil
    end
    response_hash['orgs'][0]['id']
  end

  def upload_file filename, bucket
    puts "upload #{bucket}/#{filename}"
    create_bucket bucket, bucket
    data = File.read(filename)
    rows = data.split("\n")
    rows
      .each_slice(10000) {|slice|
        @write_api.write(data: slice.join("\n"), bucket: bucket, org: 'statastic')
      }
  end

end

influx = Influx.new
Dir.children("tmp")
        .select{|d| File.directory?("tmp/#{d}")}
        .each { |d|
          Dir.children("tmp/#{d}")
             .collect { |f| "tmp/#{d}/#{f}" }
             .select { |f| File.file?(f) }
             .select { |f |  /.*\.influx$/ =~ f}
             .each { |f|
               influx.upload_file f, d
             }
        }
