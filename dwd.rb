# Copyright 2015-2022 Robert Schmidl
#
#    Licensed under the Apache License, Version 2.0 (the "License");
#    you may not use this file except in compliance with the License.
#    You may obtain a copy of the License at
#
#        http://www.apache.org/licenses/LICENSE-2.0
#
#    Unless required by applicable law or agreed to in writing, software
#    distributed under the License is distributed on an "AS IS" BASIS,
#    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#    See the License for the specific language governing permissions and
#    limitations under the License.

require 'net/ftp'
require 'zip'
require 'csv'
require 'date'
require 'fileutils'
require "open-uri"

module DwdDataType
  RECENT="recent"
  HISTORCIAL="historical"
end

# https://opendata.dwd.de/climate_environment/CDC/observations_germany/climate/daily/kl/recent/
class Dwd
  DWD_FOLDER = 'tmp/dwd'
  DWD_METADATA = '{
  "name": "DWD",
  "description": "Deutscher Wetterdienst",
  "datasets": [
    {
      "title": "Wetter",
      "description": "Klimatologische Stationsdaten (Temperatur, Druck, Niederschlag, Wind, Sonnenscheindauer etc), Qualitätskontrolle noch nicht vollständig durchlaufen",
      "source": "https://opendata.dwd.de",
      "License": "Free to use, see ftp://ftp-cdc.dwd.de/pub/CDC/Terms_of_use.txt",
      "Attribution:": "Deutscher Wetterdienst",
      "tags": {
        "01981": "Hamburg",
        "03147": "Mallersdorf-Pfaffenberg",
        "01975": "Hamburg-Fuhlsbüttel",
        "03379": "München-Stadt"
      }
    }
  ]
}'
  NAME_MAPPING = {
      'FX' => 'WINDSPITZE_MAXIMUM',
      'FM' => 'WINDGESCHWINDIGKEIT',
      'RSK' => 'NIEDERSCHLAGSHOEHE',
      'RSKF' => 'NIEDERSCHLAGSHOEHE_IND',
      'SDK' => 'SONNENSCHEINDAUER',
      'SHK_TAG' => 'SCHNEEHOEHE',
      'NM' => 'BEDECKUNGSGRAD',
      'VPM' => 'DAMPFDRUCK',
      'PM' => 'LUFTDRUCK_STATIONSHOEHE',
      'TMK' => 'LUFTTEMPERATUR',
      'UPM' => 'REL_FEUCHTE',
      'TXK' => 'LUFTTEMPERATUR_MAXIMUM',
      'TNK' => 'LUFTTEMPERATUR_MINIMUM',
      'TGK' => 'LUFTTEMP_AM_ERDB_MINIMUM'
  }

  def download station_nr
    FileUtils.mkdir_p("#{DWD_FOLDER}")

    url = "https://opendata.dwd.de//climate_environment/CDC/observations_germany/climate/daily/kl/recent/tageswerte_KL_#{station_nr}_akt.zip"
    data =  URI.parse(url).read
    open("#{DWD_FOLDER}/tageswerte_KL_#{station_nr}.zip", "wb") do |file|
      file.write(data)
    end
    "#{DWD_FOLDER}/tageswerte_KL_#{station_nr}.zip"
  end

  def unzip(file_name, station_nr, dwd_data_type)
    csv_file = "#{DWD_FOLDER}/produkt_klima_#{station_nr}_#{dwd_data_type}.txt"
    Zip::File.open(file_name) do |zipfile|
      zipfile.each do |file|
        if file.name =~ /produkt_klima_/
          zipfile.extract(file, csv_file)
        end
      end
    end
    csv_file
  end

  def to_hash(csv_file)
    data_rows = Hash.new
    CSV.open(csv_file, headers: true, col_sep: ";").map do |row|
      data_row = Hash.new
      row.each do |k, v|
        k = k.strip
        if k == "MESS_DATUM"
          data_rows[Date.parse(v).strftime("%Y-%m-%d")] = data_row
        elsif k != "eor"
          data_row[NAME_MAPPING[k]? NAME_MAPPING[k] : k] = v.strip;
        end
      end
    end
    data_rows.to_hash
  end

  def create_influx station_nr, station_name, dwd_data_type
    local_file_name = download station_nr
    csv_file = unzip(local_file_name, station_nr, dwd_data_type)
    data = to_hash(csv_file)

    File.open("#{DWD_FOLDER}/DWD_Tageswerte_#{station_name}_#{dwd_data_type}.influx", 'w') do |file|
      data.each { |k,v|
        file.write "wetterdaten,station=#{v['STATIONS_ID']} lufttemperatur=#{v['LUFTTEMPERATUR']},niederschlag=#{v['NIEDERSCHLAGSHOEHE']},sonnenscheindauer=#{v['SONNENSCHEINDAUER']} #{DateTime.parse(k).to_time.to_i}\n"
      }
    end
  end

  def create_metadata_file
    File.open("#{DWD_FOLDER}/dwd_metadata.json", 'w') do |file|
        file.write DWD_METADATA
    end
  end
end

FileUtils.rm_rf Dwd::DWD_FOLDER

dwd = Dwd.new

([["01981", "Hamburg"],
  ["03147", "Mallersdorf-Pfaffenberg"],
  ["01975", "Hamburg-Fuhlsbüttel"],
  ["03379", "München-Stadt"]]).each do |station|
  dwd.download station[0]
  dwd.create_influx station[0], station[1], DwdDataType::RECENT
end

dwd.create_metadata_file
