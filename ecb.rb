# Copyright 2015-2022 Robert Schmidl
#
#    Licensed under the Apache License, Version 2.0 (the "License");
#    you may not use this file except in compliance with the License.
#    You may obtain a copy of the License at
#
#        http://www.apache.org/licenses/LICENSE-2.0
#
#    Unless required by applicable law or agreed to in writing, software
#    distributed under the License is distributed on an "AS IS" BASIS,
#    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#    See the License for the specific language governing permissions and
#    limitations under the License.

require 'net/http'
require 'zip'
require 'csv'
require 'json'
require "open-uri"

class Ecb

  ECB_FOLDER = 'tmp/ecb'
  ECB_METADATA = '{
  "name": "ECB",
  "description": "European Central Bank",
  "datasets": [
    {
      "title": "Exchange Rates",
      "description": "Euro foreign exchange reference rates",
      "source": "https://www.ecb.europa.eu/stats/eurofxref/eurofxref-hist.zip",
      "License": "Proprietary License, free to use, see http://www.ecb.europa.eu/home/disclaimer/html/index.en.html#c",
      "Attribution:": "European Central Bank, Frankfurt am Main, Germany",
      "tags": {
        "USD": "USD",
        "JPY": "JPY",
        "BGN": "BGN",
        "CYP": "CYP",
        "CZK": "CZK",
        "DKK": "DKK",
        "EEK": "EEK",
        "GBP": "GBP",
        "HUF": "HUF",
        "LTL": "LTL",
        "LVL": "LVL",
        "MTL": "MTL",
        "PLN": "PLN",
        "ROL": "ROL",
        "RON": "RON",
        "SEK": "SEK",
        "SIT": "SIT",
        "SKK": "SKK",
        "CHF": "CHF",
        "ISK": "ISK",
        "NOK": "NOK",
        "HRK": "HRK",
        "RUB": "RUB",
        "TRL": "TRL",
        "TRY": "TRY",
        "AUD": "AUD",
        "BRL": "BRL",
        "CAD": "CAD",
        "CNY": "CNY",
        "HKD": "HKD",
        "IDR": "IDR",
        "ILS": "ILS",
        "INR": "INR",
        "KRW": "KRW",
        "MXN": "MXN",
        "MYR": "MYR",
        "NZD": "NZD",
        "PHP": "PHP",
        "SGD": "SGD",
        "THB": "THB",
        "ZAR": "ZAR"
      }
    }
  ]
}'

  def scrape
    init
    download
    unpack
    convert
    create_metadata_file
  end

  def init
    FileUtils.rm_rf("#{ECB_FOLDER}")
    FileUtils.mkdir_p("#{ECB_FOLDER}")
  end

  def download
    data =  URI.parse("https://www.ecb.europa.eu/stats/eurofxref/eurofxref-hist.zip").read
    open("#{ECB_FOLDER}/eurofxref-hist.zip", "wb") do |file|
      file.write(data)
    end
    puts Dir.children ECB_FOLDER
  end

  def unpack
    csv_file = "#{ECB_FOLDER}/EuroExchangeRates.csv"
    Zip::File.open("#{ECB_FOLDER}/eurofxref-hist.zip", "w") do |zipfile|
      zipfile.each do |file|
        if file.name =~ /eurofxref-hist.csv/
          zipfile.extract(file, csv_file)
        end
      end
    end
  end

  def convert
    File.open("#{ECB_FOLDER}/ecb_exchangerates.influx", 'w') do |file|
      CSV.open("#{ECB_FOLDER}/EuroExchangeRates.csv", headers: true, col_sep: ",").map do |row|
        timestamp = Date.parse(row['Date']).to_time.to_i
        row
          .select { |k, v| !k.nil? && k != "Date" && v != "N/A" }
          .each do |k, v|
          file.write("ecb_exchange_rates,currency=#{k} exchangerate=#{v} #{timestamp}\n")
        end
      end
    end
  end

  def create_metadata_file
    File.open("#{ECB_FOLDER}/ecb_metadata.json", 'w') do |file|
      file.write ECB_METADATA
    end
  end
end

ecb = Ecb.new
ecb.scrape
